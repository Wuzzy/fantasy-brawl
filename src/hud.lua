local saved_huds = {} -- pl_name = {hud_name = id}


function fbrawl.generate_HUD(arena, pl_name)
  if saved_huds[pl_name] then return end

  local player = minetest.get_player_by_name(pl_name)

  local stats_bg
  local kills
  local deaths
  local status_pointer
  local waypoint

  stats_bg = player:hud_add({
    hud_elem_type = "image",
    position  = {x = 1, y = 0.5},
    offset = {x = -25*3 + -25*2/3, y = 0},
    text      = "fbrawl_hud_stats.png",
    alignment = { x = 1, y=0.5},
    scale     = { x = 3, y = 3},
    number    = 0xFFFFFF,
  })

  kills = player:hud_add({
    hud_elem_type = "text",
    position  = {x = 1, y = 0.5},
    offset = {x = -53, y = 27},
    text      = 0,
    number    = 0xFFFFFF,
  })

  deaths = player:hud_add({
    hud_elem_type = "text",
    position  = {x = 1, y = 0.5},
    offset = {x = -53, y = 107.5},
    text      = 0,
    number    = 0xFFFFFF,
  })

  status_pointer = player:hud_add({
    name = "pointer_status",
    hud_elem_type = "image",
    text = "fbrawl_transparent.png",
    position = {x=0.5, y=0.5},
    offset = {x=-18.75, y = 0},
    scale = {x=2.5, y=2.5},
    alignment = {x=0.5, y=0.5},
  })

  waypoint = player:hud_add({
    name = "waypoint",
    hud_elem_type = "image_waypoint",
    text = "fbrawl_smash_item.png",
    scale = {x=0, y=0},
    size = {x=1, y=1},
  })

  local imgs_scale = 4
  
  local pl_name_y_offset = 80

  local crown_x_offset = 16*imgs_scale + 300 
  local crown_y_offset = 16*imgs_scale
  
  local scores_scale = imgs_scale - 1
  local score_x_offset = 14*scores_scale + 8
  local score_y_offset = 18*scores_scale + pl_name_y_offset
  local score_value_y_offset = 14
  local score_value_x_offset = 16

  local panel = Panel:new("fbrawl:podium", {
    player = pl_name,
    position = {x=0.5, y=0.15},
    alignment = {x=0, y=0.5},
    --bg = "fbrawl_transparent.png",
    bg_scale = {x = 1000, y = 1000},

    -- IMAGES
    sub_img_elems = {
      iron_crown = {
        scale = {x = imgs_scale, y = imgs_scale},
        offset = {x = -crown_x_offset, y = crown_y_offset},
        text = "fbrawl_iron_crown.png",
      },
      iron_kills = {
        scale = {x = scores_scale, y = scores_scale},
        offset = {x = -crown_x_offset - score_x_offset, y = crown_y_offset + score_y_offset},
        text = "fbrawl_hud_kills_score.png",
      },
      iron_deaths = {
        scale = {x = scores_scale, y = scores_scale},
        offset = {x = -crown_x_offset + score_x_offset, y = crown_y_offset + score_y_offset},
        text = "fbrawl_hud_deaths_score.png",
      },
      
      golden_crown = {
        scale = {x = imgs_scale, y = imgs_scale},
        offset = {x = 0, y = 0},
        text = "fbrawl_golden_crown.png"
      },
      golden_kills = {
        scale = {x = scores_scale, y = scores_scale},
        offset = {x = -score_x_offset, y = score_y_offset},
        text = "fbrawl_hud_kills_score.png",
      },
      golden_deaths = {
        scale = {x = scores_scale, y = scores_scale},
        offset = {x = score_x_offset, y = score_y_offset},
        text = "fbrawl_hud_deaths_score.png",
      },

      bronze_crown = {
        scale = {x = imgs_scale, y = imgs_scale},
        offset = {x = crown_x_offset, y = crown_y_offset},
        text = "fbrawl_bronze_crown.png"
      },
      bronze_kills = {
        scale = {x = scores_scale, y = scores_scale},
        offset = {x = crown_x_offset - score_x_offset, y = crown_y_offset + score_y_offset},
        text = "fbrawl_hud_kills_score.png",
      },
      bronze_deaths = {
        scale = {x = scores_scale, y = scores_scale},
        offset = {x = crown_x_offset + score_x_offset, y = crown_y_offset + score_y_offset},
        text = "fbrawl_hud_deaths_score.png",
      },
    },

    -- TEXTS
    sub_txt_elems = {
      iron_pl_name = {
        offset = {x = -crown_x_offset, y = crown_y_offset + pl_name_y_offset},
        text = "-",
        size = {x=2}
      },
      iron_kills_value = {
        offset = {x = -crown_x_offset - score_x_offset + score_value_x_offset, y = crown_y_offset + score_y_offset + score_value_y_offset},
        text = "-",
      },
      iron_deaths_value = {
        offset = {x = -crown_x_offset + score_x_offset - score_value_x_offset, y = crown_y_offset + score_y_offset + score_value_y_offset},
        text = "-",
      },

      golden_pl_name = {
        offset = {x = 0, y = pl_name_y_offset},
        text = "-",
        size = {x=2}
      },
      golden_kills_value = {
        offset = {x = -score_x_offset + score_value_x_offset, y = score_y_offset + score_value_y_offset},
        text = "-",
      },
      golden_deaths_value = {
        offset = {x = score_x_offset - score_value_x_offset, y = score_y_offset + score_value_y_offset},
        text = "-",
      },

      bronze_pl_name = {
        offset = {x = crown_x_offset, y = crown_y_offset + pl_name_y_offset},
        text = "-",
        size = {x=2}
      },
      bronze_kills_value = {
        offset = {x = crown_x_offset - score_x_offset + score_value_x_offset, y = crown_y_offset + score_y_offset + score_value_y_offset},
        text = "-",
      },
      bronze_deaths_value = {
        offset = {x = crown_x_offset + score_x_offset - score_value_x_offset, y = crown_y_offset + score_y_offset + score_value_y_offset},
        text = "-",
      },
    }
  })
  panel:hide()

  saved_huds[pl_name] = {
    stats_bg = stats_bg,
    kills = kills,
    deaths = deaths,
    status_pointer = status_pointer,
    waypoint = waypoint
  }
end



function fbrawl.update_hud(pl_name, field, new_value)
  if saved_huds[pl_name] and saved_huds[pl_name][field] then
    local player = minetest.get_player_by_name(pl_name)
    player:hud_change(saved_huds[pl_name][field], "text", new_value)
  end
end



function fbrawl.remove_huds(pl_name)
  minetest.after(1, function()
    local player = minetest.get_player_by_name(pl_name)
    
    if not player or not saved_huds[pl_name] then return end

    for name, id in pairs(saved_huds[pl_name]) do
      player:hud_remove(id)
    end
    
    if panel_lib.has_panel(pl_name, "fbrawl:podium") then
      panel_lib.get_panel(pl_name, "fbrawl:podium"):remove()
    end
    
    saved_huds[pl_name] = nil
  end)
end



function fbrawl.add_temp_hud(pl_name, hud, time)
  local player = minetest.get_player_by_name(pl_name)
  
  hud = player:hud_add(hud)
  saved_huds[pl_name] = saved_huds[pl_name] or {}
  saved_huds[pl_name][tostring(hud)] = hud

  minetest.after(time, function()
    -- Removing the hud if the player still has it.
    if saved_huds[pl_name] and saved_huds[pl_name][tostring(hud)] then
      player:hud_remove(hud)
      saved_huds[pl_name][tostring(hud)] = nil
    end
  end)

  return hud
end



function fbrawl.add_hud(pl_name, name, def)
  local player = minetest.get_player_by_name(pl_name)
  
  if not player then return end

  local hud = player:hud_add(def)
  saved_huds[pl_name] = saved_huds[pl_name] or {}
  saved_huds[pl_name][name] = hud

  return hud
end



function fbrawl.remove_hud(pl_name, name)
  local player = minetest.get_player_by_name(pl_name)
    
  if not player or not saved_huds[pl_name] or not saved_huds[pl_name][name] then return end
  
  player:hud_remove(saved_huds[pl_name][name])

  saved_huds[pl_name][name] = nil
end



function fbrawl.get_hud(pl_name, name)
  local player = minetest.get_player_by_name(pl_name)
    
  if not player or not saved_huds[pl_name] or not saved_huds[pl_name][name] then return end
  
  return saved_huds[pl_name][name]
end




controls.register_on_press(function(player, control_name)
	local pl_name = player:get_player_name()
  local mod = arena_lib.get_mod_by_player(pl_name)
  local arena = arena_lib.get_arena_by_player(pl_name)

  if mod == "fantasy_brawl" and arena.in_game and control_name == "aux1" then
    fbrawl.show_podium_HUD(pl_name)
  end
end)



controls.register_on_release(function(player, control_name)
	local pl_name = player:get_player_name()
  local mod = arena_lib.get_mod_by_player(pl_name)
  local arena = arena_lib.get_arena_by_player(pl_name)

  if 
    mod == "fantasy_brawl" and arena.in_game 
    and not arena.in_celebration and control_name == "aux1" 
  then
    if not panel_lib.has_panel(pl_name, "fbrawl:podium")  then return end
    local panel = panel_lib.get_panel(pl_name, "fbrawl:podium")
    panel:hide()
  end
end)



function fbrawl.show_podium_HUD(pl_name)
  if not panel_lib.has_panel(pl_name, "fbrawl:podium") then return end

  local panel = panel_lib.get_panel(pl_name, "fbrawl:podium")  
  
  fbrawl.update_score_hud(pl_name)
  panel:show()
end