minetest.register_privilege("fbrawl_admin", {  
   description = "It allows you to use /fbrawl"
})



ChatCmdBuilder.new("fbrawl", function(cmd)

   cmd:sub("create :arena", function(name, arena_name)
       arena_lib.create_arena(name, "fantasy_brawl", arena_name)
   end)



   cmd:sub("create :arena :minplayers:int :maxplayers:int", function(name, arena_name, min_players, max_players)
       arena_lib.create_arena(name, "fantasy_brawl", arena_name, min_players, max_players)
   end)



   cmd:sub("remove :arena", function(name, arena_name)
       arena_lib.remove_arena(name, "fantasy_brawl", arena_name)
   end)

   
   
   -- list of the arenas
   cmd:sub("list", function(name)
       arena_lib.print_arenas(name, "fantasy_brawl")
   end)



   cmd:sub("info :arena", function(name, arena_name)
       arena_lib.print_arena_info(name, "fantasy_brawl", arena_name)
   end)



   -- This sets the spawns using the player position.
   cmd:sub("setspawn :arena", function(name, arena)
       arena_lib.set_spawner(name, "fantasy_brawl", arena)
   end)



   -- This sets the arena sign.
   cmd:sub("setsign :arena", function(sender, arena)
       arena_lib.set_sign(sender, nil, nil, "fantasy_brawl", arena)
   end)


   
   cmd:sub("edit :arena", function(sender, arena)
       arena_lib.enter_editor(sender, "fantasy_brawl", arena)
   end)



   cmd:sub("enable :arena", function(name, arena)
       arena_lib.enable_arena(name, "fantasy_brawl", arena)
   end)



   cmd:sub("disable :arena", function(name, arena)
       arena_lib.disable_arena(name, "fantasy_brawl", arena)
   end)



   -- Debug commands:
   cmd:sub("play :sound :gain:number", function(pl_name, sound, gain)
       minetest.sound_play(sound, { pos = minetest.get_player_by_name(pl_name):get_pos(), gain = gain})
   end)



   cmd:sub("logs :arena", function(pl_name, arena)
      fbrawl.print_logs(arena, pl_name)
   end)

end, {
 description = [[
    
   ADMIN COMMANDS
   (Use /help fbrawl to read it all)

   Use this to configure your arena:
   - tutorial
   - create <arena name> [min players] [max players]
   - edit <arena name> 
   - enable <arena name>
   
   Other commands:
   - list
   - info <arena name>
   - remove <arena name>
   - disable <arena name>
   - logs <arena name>
   ]],
 privs = { fbrawl_admin = true }
})
