local function set_hud() end

local after = minetest.after
local delete_particlespawner = minetest.delete_particlespawner
local add_particlespawner = minetest.add_particlespawner

local base_hotbar_text = "fbrawl_hotbar.png"
local max_yellow_recharge_seconds = 2
local T = fbrawl.T



function fbrawl.show_skill_status_hud(arena)
	if not arena or not arena.in_game then return end
	
	for pl_name, props in pairs(arena.players) do
		local slots = {}
		local player = minetest.get_player_by_name(pl_name)
		local inv = player:get_inventory()
		local list = inv:get_list("main")
		local selected_slot_texture = "gui_hotbar_selected.png"

		-- for each skill in the hotbar determines which color to assign its slot
		for i, itemstack in ipairs(list) do
			local skill_name = itemstack:get_name():gsub("fantasy_brawl", "fbrawl")
			local skill = pl_name:get_skill(skill_name)

			if skill then
				-- first item has a right click and could have a left lick skill, but it's not a skill itself
				if i == 1 then
					local right_skill = pl_name:get_skill(skill.rightclick_skill)
					local left_skill = pl_name:get_skill(skill.leftclick_skill)

					selected_slot_texture = set_hud(arena, right_skill, pl_name, slots, i, selected_slot_texture, "right") or selected_slot_texture
					selected_slot_texture = set_hud(arena, left_skill, pl_name, slots, i, selected_slot_texture, "left") or selected_slot_texture
				else
					selected_slot_texture = set_hud(arena, skill, pl_name, slots, i, selected_slot_texture) or selected_slot_texture
				end
			end
		end
		
		if selected_slot_texture ~= player:hud_get_hotbar_selected_image() then
			player:hud_set_hotbar_selected_image(selected_slot_texture)
		end

		-- apply the final hotbar texture
		local final_hotbar_tex = base_hotbar_text

		for i, slot_tex in ipairs(slots) do
			final_hotbar_tex = final_hotbar_tex .. "^" .. slot_tex
		end

		player:hud_set_hotbar_image(final_hotbar_tex)
	end

	after(0, function ()
		fbrawl.show_skill_status_hud(arena)
	end)
end



function set_hud(arena, skill, pl_name, slots, i, selected_slot_texture, side)
	local arena = arena_lib.get_arena_by_player(pl_name)

	if not arena_lib.get_mod_by_player(pl_name) == "fantasy_brawl" or not skill then 
		return 
	end

	local player = minetest.get_player_by_name(pl_name)

	local pointer_hud = fbrawl.get_hud(pl_name, "status_pointer")
	local pointer_hud_table = player:hud_get(pointer_hud)
	local slot_color
	local remaining_recharge_seconds = 0
	local wielded_idx = player:get_wield_index()
	local wielded_name = player:get_wielded_item():get_name()

	local waypoint = fbrawl.get_hud(pl_name, "waypoint")
	local waypoint_table = player:hud_get(waypoint)

	-- probably an async-caused crash
	if not waypoint_table or not pointer_hud_table then return end

	-- determines remaining_seconds_to_recharge
	if skill.cooldown_timer > 0 then
		remaining_recharge_seconds = skill.cooldown_timer
	end
	if i == 4 then -- when skill is an ultimate
		remaining_recharge_seconds = fbrawl.min_kills_to_use_ultimate - arena.players[pl_name].ultimate_recharge
	end
	
	-- showing ultimate kills left if wielding it
	if arena.initial_time - arena.current_time > 5 then
		if remaining_recharge_seconds > 0 and wielded_idx == 4 and i == 4 then
			arena_lib.HUD_send_msg("broadcast", pl_name, T("@1 kills left to use the ultimate", remaining_recharge_seconds), 0.1)
		end
	end

	-- modifying the itemstack count so that it matches the cooldown
	if remaining_recharge_seconds > 0 then
		local item = fbrawl.get_class_by_skill(skill.internal_name).items[i]

		if side ~= "left" then -- unless this is the main weapon attack
			fbrawl.replace_slot_item(player, i, item, math.max(1, remaining_recharge_seconds)) 
		end
	end

	-- red or yellow slot?
	if remaining_recharge_seconds > 0 and remaining_recharge_seconds <= max_yellow_recharge_seconds then
		slot_color = "yellow"
	elseif remaining_recharge_seconds > max_yellow_recharge_seconds then
		slot_color = "red"
	end

	-- appends a yellow or red slot to slots and changes the pointer HUD
	if i == 1 and slot_color then -- the main weapon
		table.insert(slots, "fbrawl_slot_"..slot_color.."_1_"..side..".png")
		
		if not string.find(pointer_hud_table.text, "fbrawl_pointer_"..slot_color) then
			player:hud_change(pointer_hud, "text", pointer_hud_table.text .."^fbrawl_pointer_"..slot_color.."_"..side..".png")
		end

		if wielded_idx == 1 then 
			selected_slot_texture = selected_slot_texture.."^fbrawl_slot_"..slot_color.."_"..side..".png"
		end
	elseif slot_color then
		if wielded_idx == i then 
			selected_slot_texture = selected_slot_texture.."^fbrawl_slot_"..slot_color..".png"
		end
		table.insert(slots, "fbrawl_slot_"..slot_color.."_"..i..".png")
	end

	-- showing cry of gaia area of effect and impact destination
	-- TODO: generalize this
	if wielded_name == "fantasy_brawl:cry_of_gaia" and i == 4 then
		local ray = fbrawl.look_raycast(player, skill.max_range)
		local pos = ray:next() or {}
		pos = pos.above
		local area_particle_spawner = fbrawl.cry_of_gaia_area_particle_spawner

		if pos and not vector.equals(waypoint_table.world_pos, pos) then
			local scale = 62 / vector.distance(pos, player:get_pos())

			player:hud_change(waypoint, "text", "fbrawl_smash_item.png")
			player:hud_change(waypoint, "scale", {x=scale, y=scale})
			player:hud_change(waypoint, "world_pos", pos)
		
			if skill.data.area_particle_spawner then
				delete_particlespawner(skill.data.area_particle_spawner)
			end

			area_particle_spawner.pos = pos
			area_particle_spawner.playername = pl_name
			skill.data.area_particle_spawner = add_particlespawner(area_particle_spawner)

		elseif not pos and not waypoint_table.scale.x ~= 0 then
			player:hud_change(waypoint, "scale", {x=0, y=0})
		end
	elseif wielded_name ~= "fantasy_brawl:cry_of_gaia" and waypoint_table.scale.x ~= 0 then
		player:hud_change(waypoint, "scale", {x=0, y=0})
	end

	-- remove the left/right pointer hud when the recharge is over 
	if i == 1 and remaining_recharge_seconds == 0 then
		local texture = pointer_hud_table.text
		texture = texture:gsub("%^fbrawl_pointer_red_"..side..".png", "")
		texture = texture:gsub("%^fbrawl_pointer_yellow_"..side..".png", "")

		player:hud_change(pointer_hud, "text", texture)
	end
		
	-- remove the left/right pointer hud when the wielded item is not the first one
	if wielded_idx ~= 1 then
		player:hud_change(pointer_hud, "text", "fbrawl_transparent.png")
	end

	return selected_slot_texture
end
