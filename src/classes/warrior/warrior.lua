local T = fbrawl.T



fbrawl.register_class("warrior", {
   name = "Warrior",
   icon = "fbrawl_warrior_icon.png",
   description = T("Punch them with your fists and finish them with your sword! If you want some melee combat, the warrior's for you."),
   physics_override = {
      speed = 2.2,
      gravity = 0.9
   },
   items = {"fantasy_brawl:sword_steel", "fantasy_brawl:warrior_jump", "fantasy_brawl:iron_skin", "fantasy_brawl:hero_fury"},
   skills = {"fbrawl:sword_steel", "fbrawl:death_twirl", "fbrawl:warrior_jump", "fbrawl:iron_skin", "fbrawl:hero_fury",},
   hp_regen_rate = 1.2
})

dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/warrior/items.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/warrior/skills/death_twirl.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/warrior/skills/sword.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/warrior/skills/warrior_jump.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/warrior/skills/iron_skin.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/warrior/skills/hero_fury.lua")
