local T = fbrawl.T



minetest.register_node("fantasy_brawl:sword_steel", {
   drawtype = "mesh",
   mesh = "fbrawl_warrior_sword.obj",
   tiles = {"fbrawl_warrior_sword_model.png"},
   groups = {fbrawl_mesh = 1, sword = 1},
   wield_scale = {x=4,y=4,z=4},
	inventory_image = "default_tool_steelsword.png",
	tool_capabilities = {
		full_punch_interval = 0.8,
		damage_groups = {fleshy=2*20},
	},
   on_place = function(itemstack, player, pointed_thing)
      local pl_name = player:get_player_name()
      local death_twirl = pl_name:get_skill("fbrawl:death_twirl")

      if death_twirl then
         death_twirl:start()
      end
   end,
   on_secondary_use = function(itemstack, player)
      local pl_name = player:get_player_name()
      local death_twirl = pl_name:get_skill("fbrawl:death_twirl")

      if death_twirl then
         death_twirl:start()
      end
   end,
   on_drop = function() return end,
})



minetest.register_craftitem("fantasy_brawl:warrior_jump", {
   inventory_image = "fbrawl_smash_skill.png",
   
   on_drop = function() return end,
   on_use =
      function(itemstack, player)
         if fbrawl.is_on_the_ground(player) then
            local pl_name = player:get_player_name()
            local warrior_jump = pl_name:get_skill("fbrawl:warrior_jump")

            if warrior_jump then
               warrior_jump:cast()
            end
         else
            skills.error(player:get_player_name(), T("You must be touching the ground!"))
         end
      end
})



minetest.register_craftitem("fantasy_brawl:smash", {
   inventory_image = "fbrawl_smash_item.png",
   
   on_drop = function() return end,
   on_use =
      function(itemstack, player)
         local pl_name = player:get_player_name()
         local jump_skill = pl_name:get_skill("fbrawl:warrior_jump")

         if jump_skill then
            local fall_force = -jump_skill.jump_force * 2

            player:add_player_velocity({x = 0, y = fall_force, z = 0})
         end
      end
})



minetest.register_craftitem("fantasy_brawl:iron_skin", {
   inventory_image = "fbrawl_iron_skin_skill.png",
   
   on_drop = function() return end,
   on_use =
      function(itemstack, player)
         local pl_name = player:get_player_name()
         local iron_skin = pl_name:get_skill("fbrawl:iron_skin")

         if iron_skin and not iron_skin.is_active then
            iron_skin:start()
         end
      end
})



minetest.register_craftitem("fantasy_brawl:perfect_combo", {
   inventory_image = "fbrawl_perfect_combo_skill.png",
   
   on_drop = function() return end,
})



minetest.register_craftitem("fantasy_brawl:hero_fury", {
   description = T("Unleash the fury of the strongest warrior alive!"),
   inventory_image = "fbrawl_hero_fury_skill.png",
   tool_capabilities = {
		full_punch_interval = 1,
		damage_groups = {fleshy=-0.5},
	},
   on_drop = function() return end,
})



minetest.register_on_punchplayer(function(player, hitter, time_from_last_punch, tool_capabilities, dir, damage)
   local pl_name = hitter:get_player_name()
   local wielded_item = hitter:get_wielded_item():get_name()

   if wielded_item == "fantasy_brawl:hero_fury" then
      fbrawl.cast_ultimate(pl_name, "fbrawl:hero_fury", player:get_player_name())
   end
end)