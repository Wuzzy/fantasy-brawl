local T = fbrawl.T



skills.register_skill("fbrawl:sword_steel", {
   name = T("Sword"),
   icon = "default_tool_steelsword.png",
   rightclick_skill = "fbrawl:death_twirl",
   description = T("Your trusted steel sword *slice slice slice*.@nRIGHT CLICK: sprint damaging whoever gets in the way.")
})