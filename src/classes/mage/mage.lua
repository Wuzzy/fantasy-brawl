local T = fbrawl.T



fbrawl.register_class("mage", {
   name = "Mage",
   description = T("Wanna crush them with meteors? Pew pew them with bubbles from far away? Then the mage is for you."),
   icon = "fbrawl_mage_icon.png",
   physics_override = {
      speed = 1.6,
   },
   items = {"fantasy_brawl:mage_staff", "fantasy_brawl:fire_sprint", "fantasy_brawl:gaia_fist", "fantasy_brawl:cry_of_gaia"},
   skills = {"fbrawl:mage_staff", "fbrawl:ice_spikes", "fbrawl:bubble_beam", "fbrawl:fire_sprint", "fbrawl:gaia_fist", "fbrawl:cry_of_gaia"},
   hp_regen_rate = 2.7
})



dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/mage/skills/staff.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/mage/skills/bubble_beam.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/mage/skills/ice_spikes.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/mage/skills/fire_sprint.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/mage/skills/cry_of_gaia.lua")
dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/mage/skills/gaia_fist.lua")

dofile(minetest.get_modpath("fantasy_brawl") .. "/src/classes/mage/items.lua")
