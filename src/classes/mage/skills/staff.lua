local T = fbrawl.T



skills.register_skill("fbrawl:mage_staff", {
   name = T("Mage Staff"),
   icon = "fbrawl_mage_staff.png",
   rightclick_skill = "fbrawl:ice_spikes",
   leftclick_skill = "fbrawl:bubble_beam",
   description = T("Cast a beam of bubbles that can hit far away enemies.@nRIGHT CLICK: create a barricade of ice spikes.")
})