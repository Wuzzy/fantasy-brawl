local T = fbrawl.T



minetest.register_node("fantasy_brawl:mage_staff", {
  drawtype = "mesh",
  mesh = "fbrawl_mage_staff.obj",
  tiles = {"fbrawl_mage_staff_model.png"}, 
  inventory_image = "fbrawl_mage_staff.png",
  groups = {fbrawl_mesh = 1},
  wield_scale = {x=3.7,y=3.7, z=3.7},
  on_drop = function() return end,
  on_place = function(itemstack, player, pointed_thing) 
    local pl_name = player:get_player_name()
    local ice_spikes = pl_name:get_skill("fbrawl:ice_spikes")

    if ice_spikes and fbrawl.is_on_the_ground(player) then
      ice_spikes:cast()
    else
      skills.error(pl_name, T("You must be touching the ground!"))
    end
  end,
  on_secondary_use = function (itemstack, player)
    local pl_name = player:get_player_name()
    local ice_spikes = pl_name:get_skill("fbrawl:ice_spikes")

    if ice_spikes and fbrawl.is_on_the_ground(player) then
      ice_spikes:cast()
    else
      skills.error(pl_name, T("You must be touching the ground!"))
    end
  end,
  on_use = function(itemstack, player)
		local pl_name = player:get_player_name()
    local bubble_beam = pl_name:get_skill("fbrawl:bubble_beam")

    if bubble_beam then
      bubble_beam:cast()
    end
  end
})



minetest.register_craftitem("fantasy_brawl:fire_sprint", {
  inventory_image = "fbrawl_fire_sprint_skill.png",
  
  on_drop = function() return end,
  on_use =
     function(itemstack, player)
        local pl_name = player:get_player_name()
        local fire_sprint = pl_name:get_skill("fbrawl:fire_sprint")

        if fire_sprint then
          fire_sprint:start()
        end
     end
})



minetest.register_craftitem("fantasy_brawl:cry_of_gaia", {
  inventory_image = "fbrawl_cry_of_gaia_skill.png",
  
  on_drop = function() return end,
  on_use =
     function(itemstack, player)
        local pl_name = player:get_player_name()
        local pl_pos = vector.add(player:get_pos(), {x=0, y=1, z=0})
        local ray = fbrawl.look_raycast(player, 200)
        local crush_point = ray:next() or {}
        crush_point = crush_point.above
        
        if not crush_point then
          skills.error(pl_name, T("You can't point it in the sky!"))
          return
        end

        fbrawl.cast_ultimate(pl_name, "fbrawl:cry_of_gaia")
     end
})



minetest.register_craftitem("fantasy_brawl:enchanted_coat", {
  inventory_image = "fbrawl_enchanted_coat_skill.png",
  
  on_drop = function() return end,
  on_use =
     function(itemstack, player)
        local pl_name = player:get_player_name()
        local enchanted_coat = pl_name:get_skill("fbrawl:enchanted_coat")

        if enchanted_coat and not enchanted_coat.is_active then
          enchanted_coat:start()
        end
     end
})



minetest.register_craftitem("fantasy_brawl:gaia_fist", {
  description = skills.get_skill_def("fbrawl:gaia_fist").description,
  inventory_image = "fbrawl_gaia_fist.png",
  
  on_drop = function() return end,
  on_use =
     function(itemstack, player)
        local pl_name = player:get_player_name()
        local gaia_fist_skill = pl_name:get_skill("fbrawl:gaia_fist")
        local pl_pos = vector.add(player:get_pos(), {x=0, y=1, z=0})

        local ray = fbrawl.look_raycast(player, 200)
        local crush_point = ray:next() or {}
        crush_point = crush_point.above
        
        if not crush_point then
          skills.error(pl_name, T("You can't point it in the sky!"))
          return
        end

        if gaia_fist_skill then
          gaia_fist_skill:cast()
        end
     end
})